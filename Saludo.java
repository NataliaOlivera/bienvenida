

public class Saludo {

    private String saludoInicial;

    public void setSaludoInicial(String saludoInicial){
        this.saludoInicial= saludoInicial;
    }

    public String getSaludoInicial(){
        return saludoInicial;
    }

    public void saludoDeMadrugada(){
        System.out.println("Hola Buenos Dias");
    }

    public void saludoDeTarde(){
        System.out.println("Hola Buenas Tardes");
    }

    private void saludoDeNoche(){
        System.out.println("Hola Buenas Noches");
    }


}